import pygame
import fn
import numpy as np

def get_pixel_array():
	pixel_array = np.full((fn.width, fn.height, 3), 0)
	for pixel in fn.pixels:
		x = round(pixel.p[0])
		y = round(pixel.p[1])
		if x > 0 and x < fn.width and y > 0 and y < fn.height:
			pixel_array[x, y, 0] = pixel.c[0]
	return pixel_array

pygame.init()
screen = pygame.display.set_mode((fn.width, fn.height))
done = False

clock = pygame.time.Clock()
fn.init()

while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True
			fn.exit()
		if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
			fn.init()

	fn.step()

	# draw the array of pixels
	pixel_array = get_pixel_array()
	pygame.surfarray.blit_array(screen, pixel_array)
	
	pygame.display.flip()
	clock.tick(fn.inverse_timestep)