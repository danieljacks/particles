import random
from math import atan, sin, cos, ceil, sqrt
from threading import Timer

dimensions = (150, 150)
width = dimensions[0]
height = dimensions[1]
pixel_count = 6
timestep = 0.001
inverse_timestep = 1 / timestep
max_timestep = 0.1
k = 1000000
frame = 0
cutoff_radius_sqr = 1500**2
max_initial_v = 0

# TODO: make update frequency directly proportional to v (and hence distance traveled). Should stop them flinging each other away
# also may need to update velocity after position, so that the velocity update can inform the time delay as to how long to use that velocity. otherwise, the velocity will be using the timestep from the previous frame
# will probably need to rewrite to have each pixel calculate its own timing independantly. Then update timing will only be for drawing.
# after that, look into having the pixels bounce with they are within 1 unit of each other, instead of just ignoring the other pixel

class Pixel(object):
    p = [0,0]
    v = [0,0]
    a = [0,0]
    c = [0,0,0]
    m = 1
    q = 1
    step_timer = [Timer(timestep, lambda x: x), Timer(timestep, lambda x: x)]
    adjusted_timestep = [timestep, timestep]
    distance_cache = [{}, {}]
    last_frame = [0, 0]
    next_frame = [0, 0]

    def __init__ (self, p, v, a, c):
        self.p = p
        self.v = v
        self.a = a
        self.c = c

        self.last_frame = [frame, frame]
        self.calcuate_next_frame(0)
        self.calcuate_next_frame(1)

    def calcuate_next_frame (self, i):
        self.adjusted_timestep[i] = max(timestep, min(max_timestep, 1 / max(1, self.v[i])))
        frame_delta = ceil(self.adjusted_timestep[i] / timestep) # round to nearest frame
        self.adjusted_timestep[i] = frame_delta * timestep # recalculate rounded frame
        self.next_frame[i] = self.last_frame[i] + frame_delta

        self.adjusted_timestep[i] = timestep
        self.next_frame[i] = self.last_frame[i] + 1

    def step(self, i):
        if self.next_frame[i] > frame: # only run on correct frame
            return

        f = 0
        for pixel in pixels:
            if round(self.p[i]) == round(pixel.p[i]):
                continue

            dx = pixel.p[0] - self.p[0]
            dy = pixel.p[1] - self.p[1]
            d_sqr = dx**2 + dy**2

            if d_sqr > cutoff_radius_sqr: # ignore far off pixels
                continue

            fh = k * self.q * pixel.q / ( d_sqr )

            # if (dx == 0):
            #     angle = 1.57079632679
            # else:
            #     angle = atan(dy / dx)

            if i == 0:
                # f1 = sign(dx) * fh * cos(angle)
                f = fh * dx / sqrt(d_sqr)
                pass
            else:
                # f2 = sign(dy) * fh * sin(angle)
                f = fh * dy / sqrt(d_sqr)
                pass

        f -= self.v[i] # friction
        f *= self.adjusted_timestep[i] # adjust for timestep

        self.p[i] += self.v[i] * self.adjusted_timestep[i]
        did_bounce = bounce(self.p, i)
        if did_bounce:
            self.v[i] *= -1
        self.a[i] = f / self.m
        self.v[i] += self.a[i] * self.adjusted_timestep[i] # velocity update is last to let the step timer adjust based on velocity before the velocity is actually used to change position

        self.calcuate_next_frame(i)


pixels = []

def init():
    global pixels
    pixels = []

    widths = random.sample(range(0, width), pixel_count)
    heights = random.sample(range(0, height), pixel_count)

    vxs = [random.randint(-max_initial_v,max_initial_v) for i in range(pixel_count)]
    vys = [random.randint(-max_initial_v,max_initial_v) for i in range(pixel_count)]
    pixels = [Pixel([widths[i],heights[i]],[vxs[i],vys[i]],[0,0],[255,255,255]) for i in range(len(widths))]

def step():
    for pixel in pixels:
        pixel.step(0)
        pixel.step(1)
    global frame
    frame += 1

def sign(n):
    if n > 0: return 1
    if n < 0: return -1
    return 0

def exit():
    pass

def outside_bounds(pos):
    return True if pos[0] < 0 or pos[0] >= width or pos[1] < 0 or pos[1] >= height else False

def bounce(p, i):
    if p[i] < 0: # below lower bound
        p[i] = -p[i]
        return True
    elif p[i] >= dimensions[i]: # above upper bound
        p[i] = 2 * dimensions[i] - p[i]
        return True
    else: # inside bounds
        return False